using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseCamera : MonoBehaviour
{

    [SerializeField] private float cameraDistance;
    [SerializeField] private float maxAngle;

    public float MaxAngle { get { return maxAngle; } }

    private GameObject followObject;
    public GameObject FollowObject { set { followObject = value; } }
    
    private GameObject board;
    public GameObject Board { set { board = value; } }

    private Vector3 angle;
    public Vector3 Angle { set { angle = value; } }

    private float angleIntensityMultiplyer;
    private bool enableAngleTilt;
    private bool enableRotationFollow;

    // Start is called before the first frame update
    void Start()
    {
        enableAngleTilt = true;
        angleIntensityMultiplyer = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (enableRotationFollow)
        {
            if (followObject != null && board != null)
            {
                transform.position = followObject.transform.position + board.transform.up * cameraDistance;
                transform.rotation = board.transform.rotation;
                transform.Rotate(new Vector3(90, 0, 0));
            }
        }
        else if (followObject != null)
        {
            transform.rotation = Quaternion.Euler(90, 0, 0);
            transform.position = followObject.transform.position + Vector3.up * cameraDistance;
            float rotateAngle = enableAngleTilt ? angle.magnitude * angleIntensityMultiplyer : 0f;
            transform.RotateAround(followObject.transform.position, new Vector3(-angle.z, 0, angle.x), rotateAngle);
        }
    }

    public void EnableAngle(bool enable)
    {
        enableAngleTilt = enable;
    }

    public void EnableRotationFollow(bool enable)
    {
        enableRotationFollow = enable;
    }

    public void SetIntensityMultiplyer(float intensity)
    {
        angleIntensityMultiplyer = intensity;
    }    
}
