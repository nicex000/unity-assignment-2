using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public RectTransform root;
    public RectTransform outerCircle;
    public RectTransform innerCircle;
    public CanvasScaler canvasScaler;
    private float maxDistance;

    void Awake()
    {
    }
    public void OnDrag(PointerEventData eventData)
    {
        innerCircle.anchoredPosition = UnscaleEventDelta(eventData.position) - (Vector2)root.anchoredPosition;
        if (innerCircle.anchoredPosition.magnitude > maxDistance)
            innerCircle.anchoredPosition = innerCircle.anchoredPosition.normalized * maxDistance;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        innerCircle.anchoredPosition = outerCircle.anchoredPosition;
    }

    public Vector2 GetScaledValue()
    {
        return innerCircle.anchoredPosition / maxDistance;
    }
    private Vector2 UnscaleEventDelta(Vector2 vec)
    {
        Vector2 referenceResolution = canvasScaler.referenceResolution;
        Vector2 currentResolution = new Vector2(Screen.width, Screen.height);

        float widthRatio = currentResolution.x / referenceResolution.x;
        float heightRatio = currentResolution.y / referenceResolution.y;
        float ratio = Mathf.Lerp(widthRatio, heightRatio, canvasScaler.matchWidthOrHeight);

        return vec / ratio;
    }

    // Start is called before the first frame update
    void Start()
    {
        maxDistance = outerCircle.rect.height / 2f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
