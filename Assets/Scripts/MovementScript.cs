using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    [SerializeField] private ChaseCamera chaseCam;
    [SerializeField] private Camera camera;
    [SerializeField] private float intensity;
    [SerializeField] private float easing;
    [SerializeField] private float gyroDeadzone;
    [SerializeField] private Joystick joystick;
    [SerializeField] private GameObject board;
    [SerializeField] private float maxBoardRotation = 30f;
    private Vector3 previousForce, camForce;
    private bool usingTouch;
    private bool usingGyro;
    private bool rotateBoard;
    // Start is called before the first frame update
    void Start()
    {
        chaseCam.FollowObject = this.gameObject;
        chaseCam.Board = this.board;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 inputForce = Vector3.zero;

        inputForce = GetForceFromJoystick();

        if (inputForce == Vector3.zero)
            inputForce = GetForceFromTouchInput();

        if (inputForce == Vector3.zero)
            inputForce = GetForceFromGyroscope(); 
        if (inputForce == Vector3.zero)
            inputForce = GetForceFromKBInput();

        var targetForce = inputForce * intensity;

        if (rotateBoard)
        {
            if (!usingGyro)
            {
                chaseCam.EnableRotationFollow(false);
                var maxDeg = maxBoardRotation;
                targetForce = Vector3.ClampMagnitude(inputForce, 1f) * maxDeg;
                previousForce = UpdatePreviousForce(previousForce, targetForce, targetForce != Vector3.zero);
                board.transform.eulerAngles = new Vector3(previousForce.z, 0f, -previousForce.x);
            }
            else
            {
                chaseCam.EnableRotationFollow(true);
                var maxDeg = 90;
                targetForce = inputForce * maxDeg;
                board.transform.eulerAngles = new Vector3(targetForce.z, 0f, -targetForce.x);
            }

        }
        else
        {
            previousForce = UpdatePreviousForce(previousForce, targetForce, targetForce != Vector3.zero);
            camForce = UpdatePreviousForce(camForce, targetForce, !usingTouch && targetForce != Vector3.zero);



            UpdateChaseCam(camForce);
        }

    }

    public void ToggleBoardRotation(bool enable)
    {
        rotateBoard = enable;
    }

    private void FixedUpdate()
    {
        if (!rotateBoard) this.GetComponent<Rigidbody>().AddForce(previousForce);
    }

    private Vector3 GetForceFromJoystick()
    {
        Vector3 force = Vector3.zero;

        Vector2 input = joystick.GetScaledValue();

        force.z = input.y;
        force.x = input.x;

        if (force != Vector3.zero)
        {
            usingTouch = false;
            usingGyro = false;
        }

        return force;
    }

    private Vector3 GetForceFromTouchInput()
    {
        Vector3 force = Vector3.zero;
        foreach (var touch in Input.touches)
        {
            var ray = camera.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, camera.nearClipPlane));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 worldPos = hit.point;
                force += (worldPos - this.transform.position);
            }
        }

        force.y = 0f;
        force.Normalize();
        if (force != Vector3.zero)
        {
            usingTouch = true;
            usingGyro = false;
        }
        return force;
    }

    private Vector3 GetForceFromGyroscope()
    {
        Vector3 force = Vector3.zero;
        Vector3 accelNoYaw = new Vector3(Input.acceleration.x, 0f, Input.acceleration.y);
        if (accelNoYaw.magnitude > gyroDeadzone)
            force += accelNoYaw;


        if (force != Vector3.zero)
        {
            usingTouch = false;
            usingGyro = true;
        }
        return force;
    }

    private Vector3 GetForceFromKBInput()
    {
        Vector3 force = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            force += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            force += Vector3.back;
        }
        if (Input.GetKey(KeyCode.A))
        {
            force += Vector3.left;
        }
        if (Input.GetKey(KeyCode.D))
        {
            force += Vector3.right;
        }

        if (force != Vector3.zero)
        {
            usingTouch = false;
            usingGyro = false;
        }

        return force;
    }


    private Vector3 UpdatePreviousForce(Vector3 outputForce, Vector3 targetForce, bool increase)
    {
        if (increase)
        {
            outputForce += targetForce.normalized * easing;
            if (outputForce.magnitude > targetForce.magnitude) outputForce = Vector3.ClampMagnitude(outputForce, targetForce.magnitude);
        }
        else
        {
            if (outputForce.magnitude < easing) outputForce = Vector3.zero;
            outputForce -= outputForce.normalized * easing;
        }

        return outputForce;
    }


    private void UpdateChaseCam(Vector3 angle)
    {
        if (Mathf.Abs(angle.x) > chaseCam.MaxAngle)
            angle.x = chaseCam.MaxAngle * Mathf.Sign(angle.x);
        if (Mathf.Abs(angle.z) > chaseCam.MaxAngle)
            angle.z = chaseCam.MaxAngle * Mathf.Sign(angle.z);
        chaseCam.Angle = angle;
    }
}
