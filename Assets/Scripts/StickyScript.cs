using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyScript : MonoBehaviour
{
    [SerializeField] private float minInDistance;
    [SerializeField] private float maxOutDistance;
    private Dictionary<int, float[]> interactionDistances;

    // Start is called before the first frame update
    void Start()
    {
        interactionDistances = new Dictionary<int, float[]>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerStay(Collider collision)
    {


        if (collision.gameObject.CompareTag("Sticky"))
        {
            int id = collision.gameObject.GetInstanceID();
            float distance = (this.transform.position - collision.transform.position).magnitude;
            if (interactionDistances.ContainsKey(id))
            {
                if (minInDistance > 0)
                {
                    if (distance < interactionDistances[id][0] - minInDistance ||
                        interactionDistances[id][0] != interactionDistances[id][1])
                    {
                        if (distance > interactionDistances[id][1] && distance > maxOutDistance)
                        {
                            collision.transform.SetParent(this.transform);
                            interactionDistances.Remove(id);
                            collision.isTrigger = false;
                            return;
                        }
                        else
                        {
                            interactionDistances[id][1] = distance;
                        }
                    }
                }
                else if (distance > interactionDistances[id][1] && distance > maxOutDistance)
                {
                    collision.transform.SetParent(this.transform);
                    interactionDistances.Remove(id);
                    collision.isTrigger = false;
                    return;
                }
                else
                {
                    interactionDistances[id][1] = distance;
                }
            }
            else
                interactionDistances.Add(id, new[]{ distance, distance});
        }
    }
}
