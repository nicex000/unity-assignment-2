# Unity Assignment 2

This is a school assignment for the Mobile class, it's just a simple Unity project.

Maze board with ball that rolls in it. Small cubes stick to the ball.

# Input methods

- Virtual Joystick
- Touch
- Phone Accelerometer
- WASD (for easy testing)

# UI

- Flag to switch between moving the ball and rotating the board on all input methods.
- Flag to toggle camera tilting.
    - slider to amplify the camera tilt effect.
